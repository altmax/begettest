use begettest;

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS temp_log;
DROP TRIGGER IF EXISTS aft_ins;
DROP TRIGGER IF EXISTS aft_upd;
DROP TRIGGER IF EXISTS aft_del;