package main

import (
	"os"
	"time"

	"database/sql"
	"fmt"
	"log"
	"net/url"
	"os/signal"
	"syscall"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	flag "github.com/spf13/pflag"
)

type TempLog struct {
	ID     int    `db:"id"`
	UserID int    `db:"user_id"`
	Action string `db:"action"`
}

var (
	mysql = flag.String("mysql", "root:coding1@tcp(localhost:3306)/begettest", "mysql dsn")
)

func main() {

	dsn := *mysql + "?parseTime=true&loc=" + url.QueryEscape("Europe/Moscow")

	res := make(chan TempLog)
	done := make(chan struct{}, 1)
	defer close(res)
	defer close(done)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGINT)

	go DBSurvey(dsn, res, done)
	for {
		select {
		case l := <-res:
			fmt.Printf("%s:%d\n", l.Action, l.UserID)
		case killSignal := <-interrupt:
			done <- struct{}{}
			if killSignal == os.Interrupt {
				fmt.Printf("\n%s\n", "was interrupted by system signal")
			} else {
				fmt.Printf("%s\n", "was killed")
			}
			os.Exit(0)
		}
	}

}

//func DBSurvey(dbconn *sqlx.DB, res chan TempLog, done <-chan struct{}) {
func DBSurvey(dsn string, res chan TempLog, done <-chan struct{}) {

	query := `select id, user_id, action from temp_log`
	for {
		logs, err := func() ([]TempLog, error) {
			dbconn, err := openDB(dsn)
			var logs []TempLog
			if err != nil {
				return logs, err
			}
			defer dbconn.Close()
			err = dbconn.Select(&logs, query)
			if err == sql.ErrNoRows {
				return logs, nil
			}
			if err != nil {
				log.Printf("%s%v\n", "[ERROR]", err)
				return logs, nil
			}
			return logs, nil
		}()
		if err != nil {
			panic(err)
		}
		if len(logs) == 0 {
			continue
		}
		for _, v := range logs {
			select {
			case <-done:
				return
			case res <- v:
				go DelRow(dsn, v.ID)
			}
		}
		time.Sleep(1000 * time.Millisecond)

	}
}

func DelRow(dsn string, id int) {
	q := "delete from temp_log where id = ?"
	dbconn, _ := openDB(dsn)
	dbconn.Exec(q, id)
	dbconn.Close()
}

func openDB(dsn string) (*sqlx.DB, error) {
	m, err := sqlx.Open("mysql", dsn)
	if err != nil {
		return nil, errors.Wrapf(err, "open mysql on address %s failed", dsn)
	}

	err = m.Ping()
	if err != nil {
		return nil, errors.Wrapf(err, "ping mysql on address %s failed", dsn)
	}

	return m, err
}
