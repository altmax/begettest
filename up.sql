use begettest;

CREATE TABLE user
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(100),
    last_name VARCHAR(100)
);

CREATE TABLE transactions
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT,
    amount BIGINT DEFAULT 0 NOT NULL
);

CREATE TABLE temp_log
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT,
    action VARCHAR(31)
);

CREATE TRIGGER aft_ins AFTER INSERT ON transactions
    FOR EACH ROW INSERT INTO temp_log SET user_id = NEW.user_id, action = 'insert';

CREATE TRIGGER aft_upd AFTER UPDATE ON transactions
    FOR EACH ROW INSERT INTO temp_log SET user_id = OLD.user_id, action = 'update';

CREATE TRIGGER aft_del AFTER DELETE ON transactions
    FOR EACH ROW INSERT INTO temp_log SET user_id = OLD.user_id, action = 'delete';